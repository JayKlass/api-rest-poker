const express = require("express");
const bodyParser = require('body-parser');
const app = express();
let functions = require('./functions');
const underscore = require('underscore');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let mano = {
  jugadas: {
    jugador: '',
    apuesta: '',
    cartas: ''
  },
  bote: ''
};


let respuesta = {
 error: false,
 codigo: 200,
 mensaje: ''
};

app.get('/', function(req, res) {
 respuesta = {
  error: true,
  codigo: 200,
  mensaje: 'Punto de inicio'
 };
 
 res.send(functions.pair(req.body));
});

app.route('/hand')
 .get(function (req, res) {
  respuesta = {
   error: false,
   codigo: 200,
   mensaje: ''
  };
  if(req.body === '') {
   respuesta = {
    error: true,
    codigo: 501,
    mensaje: 'No hay ninguna jugada'
   };
  } else {
   respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'respuesta',
    respuesta: req.body
   };
  }
  res.send(respuesta);
 })
 .post(function (req, res) {
    if(functions.comprobacionManos(req.body)) {
    respuesta = {
      error: true,
      codigo: 502,
      mensaje: 'Es obligatorio especificar las jugadas y el bote y que los valores sean validos'
    };
   } else {
    respuesta = {
     error: false,
     codigo: 200,
     mensaje: 'Jugada realizada',
    };
   }
   res.send(functions.pair(req.body));
 })

app.use(function(req, res, next) {
 respuesta = {
  error: true, 
  codigo: 404, 
  mensaje: 'URL no encontrada'
 };
 res.status(404).send(respuesta);
});
app.listen(8002, () => {
 console.log("El servidor está inicializado en el puerto 8002");
});