

function pair(mano) {
  let recivedHand = mano;
  let mapWithNumericValues = new Map();
  let mapWithSuitValues = new Map();
  let mapWithCards = new Map();
  
  
  let result = [];

  for (let i = 0; i < recivedHand.length; i++) {
    let boteAcumulado = 0;
    let playedMoney = 0;
    for (let j = 0; j < recivedHand[i].jugadas.length; j++) {
      let acumulator = 0;
      let numericValueOfCards = [];
      let suitValueOfCards = [];
      for (let k = 0; k < recivedHand[i].jugadas[j].cartas.length; k++) {

        if (recivedHand[i].jugadas[j].cartas[k].valor == 'J') {
          recivedHand[i].jugadas[j].cartas[k].valor = '11';;
        } else if (recivedHand[i].jugadas[j].cartas[k].valor == 'Q') {
          recivedHand[i].jugadas[j].cartas[k].valor = '12';;
        } else if (recivedHand[i].jugadas[j].cartas[k].valor == 'K') {
          recivedHand[i].jugadas[j].cartas[k].valor = '13';
        } else if (recivedHand[i].jugadas[j].cartas[k].valor == 'A') {
          recivedHand[i].jugadas[j].cartas[k].valor = '14';
        }

        //Almaceno en un array unicamente los valores numericos de las cartas
        numericValueOfCards.push(recivedHand[i].jugadas[j].cartas[k].valor);

        //Almaceno en un array unicamente los palos de las cartas
        suitValueOfCards.push(recivedHand[i].jugadas[j].cartas[k].palo);
      }

      //Voy almacenando el dinero que se va jugando
      //Se almacena en un mapa cada jugador con el valor numerico de sus cartas
      playedMoney += parseInt(recivedHand[i].jugadas[j].apuesta);
      mapWithNumericValues.set(recivedHand[i].jugadas[j].jugador, numericValueOfCards);
      mapWithSuitValues.set(recivedHand[i].jugadas[j].jugador, suitValueOfCards);
      mapWithCards.set(recivedHand[i].jugadas[j].jugador, recivedHand[i].jugadas[j].cartas);
    }

    boteAcumulado += parseInt(recivedHand[i].bote) + parseInt(playedMoney);

    if (comprobacionManos(mano)) {
      result.push('Partida amañada');
    } else if(winnerWithPairs(mapWithNumericValues, mapWithSuitValues, mapWithCards) == 'Empate') {
      result.push('Empate');
    } else {
      result.push(`${winnerWithPairs(mapWithNumericValues, mapWithSuitValues, mapWithCards)} gana ${boteAcumulado} euros`);
    }
    
  }

  //boteAcumulado += parseInt(playedMoney);

 /* if (comprobacionManos(mano)) {
    return 'Partida amañada'
  } else {
    return `${winnerWithPairs(mapWithNumericValues, mapWithSuitValues, mapWithCards)} gana ${boteAcumulado} euros`;
  }*/

  return result;
}

//Funcion que evalua que jugador ha ganado
function winnerWithPairs(mapWithNumericValues, mapWithSuitValues, mapWithCards) {

  //Declaracion de variables
  let carta;
  let pairMap = new Map();
  let tripleMap = new Map();
  let pokerMap = new Map();
  let evaluatePairs = new Map();
  let straightMap = new Map();
  let colorMap = new Map();
  let fullMap = new Map();
  let royalStraightMap = new Map();
  let winnerCounter = 0;
  let winnerPlayer;
  let noCombinations = false;

  //Funcion que ordena el mapa inicial de jugador / cartas de cara a evaluar las escaleras
  for (let [clave, valor] of mapWithNumericValues.entries()) {
    valor.sort(comparar);
  }

  //Encontrar parejas
  for (let [clave, valor] of mapWithNumericValues.entries()) {
    pairMap.set(clave, findDuplicatesInArray(valor));
  }

  //Encontrar trios
  for (let [clave, valor] of mapWithNumericValues.entries()) {
    tripleMap.set(clave, findTriplicatesInArray(valor));
  }

  //Encontrar poker
  for (let [clave, valor] of mapWithNumericValues.entries()) {
    pokerMap.set(clave, findPokerInArray(valor));
  }

  //Encontrar escaleras
  for (let [clave, valor] of mapWithNumericValues.entries()) {
    straightMap.set(clave, findStraightInArray(valor));
  }

  //Encontrar full
  for (let [clave, valor] of mapWithNumericValues.entries()) {
    fullMap.set(clave, findFullInArray(valor));
  }

  //Encontrar color
  for (let [clave, valor] of mapWithSuitValues.entries()) {
    colorMap.set(clave, findColorInArray(valor));
  }

  //Encontrar escaleras de color
  for (let [clave, valor] of mapWithCards.entries()) {
    royalStraightMap.set(clave, findRoyalStraightInArray(valor));
  }

  //Asignar dependiendo de si hay una doble pareja, solo una pareja o nada
  for (let [clave, valor] of pairMap.entries()) {
    if (valor.length > 0 && valor.length < 2) {
      evaluatePairs.set(clave, 2);
    } else if (valor.length == 2) {
      evaluatePairs.set(clave, 3);
    } else {
      evaluatePairs.set(clave, 1);
    }
  }

  //Asignar los trios
  for (let [clave, valor] of tripleMap.entries()) {
    if (valor.length >= 1) {
      evaluatePairs.set(clave, 4);
    }
  }

  //Asignar escalera
  for (let [clave, valor] of straightMap.entries()) {
    if (valor.length >= 1) {
      evaluatePairs.set(clave, 5);
    }
  }

  //Asignar color
  for (let [clave, valor] of colorMap.entries()) {
    if (valor.length >= 1) {
      evaluatePairs.set(clave, 6);
    }
  }

  //Asignar full
  for (let [clave, valor] of fullMap.entries()) {
    if (valor.length >= 1) {
      evaluatePairs.set(clave, 7);
    }
  }

  //Asignar los pokers
  for (let [clave, valor] of pokerMap.entries()) {
    if (valor.length >= 1) {
      evaluatePairs.set(clave, 8);
    }
  }

  //Asignar escaleras de color
  for (let [clave, valor] of royalStraightMap.entries()) {
    if (valor.length >= 1) {
      evaluatePairs.set(clave, 9);
    }
  }

  //console.log(evaluatePairs);


  // Se ordena el mapa por value, para que en caso de que suceda algo parecido a esto funcione:
  // 1º jugador pareja, 2º jugador pareja, 3º jugador trio.
  // Map: jug1 - 2, jug2 - 2, jug3 - 3
  // Al ser los dos primeros values iguales, entraria en el primer elseif y no tendria en cuenta el trio
  const mapSort1 = new Map([...evaluatePairs.entries()].sort((a, b) => b[1] - a[1]));

  //Evaluar ganador segun la mano recibida
  for (let [clave, valor] of mapSort1.entries()) {
    if (valor > winnerCounter) {
      winnerCounter = valor;
      winnerPlayer = clave;
      noCombinations = false;
    } else if (valor == winnerCounter && winnerCounter != 0 && valor >= 2 && valor <= 3) {
      return winnerWithMaximumValueCard(pairMap);
    } else if (valor == winnerCounter && winnerCounter != 0 && valor == 4) {
      return winnerWithMaximumValueCard(tripleMap);
    } else if (valor == winnerCounter && winnerCounter != 0 && valor == 5) {
      return winnerWithMaximumValueCard(straightMap);
    } else if (valor == winnerCounter && winnerCounter != 0 && valor == 6) {
      return winnerWithMaximumValueCard(mapWithNumericValues);
    } else if (valor == winnerCounter && winnerCounter != 0 && valor == 7) {
      return winnerWithMaximumValueCard(fullMap);
    } else if (valor == winnerCounter && winnerCounter != 0 && valor == 8) {
      return winnerWithMaximumValueCard(pokerMap);
    } else if (valor == winnerCounter && winnerCounter != 0 && valor == 9) {
      return winnerWithMaximumValueCard(mapWithNumericValues);
    } else if (valor == 1) {
      return winnerWithMaximumValueCard(mapWithNumericValues);
    }
  }

  return winnerPlayer;
}

//Funcion para encontrar parejas
function findDuplicatesInArray(arra1) {
  let object = {};
  let result = [];

  arra1.forEach(function (item) {
    if (!object[item])
      object[item] = 0;
    object[item] += 1;
  })

  for (let prop in object) {
    if (object[prop] == 2) {
      result.push(prop);
    }
  }
  return result;
}

//Funcion para encontrar trios
function findTriplicatesInArray(arra1) {
  let object = {};
  let result = [];

  arra1.forEach(function (item) {
    if (!object[item])
      object[item] = 0;
    object[item] += 1;
  })

  for (let prop in object) {
    if (object[prop] == 3) {
      result.push(prop);
    }
  }

  return result;

}

//Funcion para encontrar pokers
function findPokerInArray(arra1) {
  let object = {};
  let result = [];

  arra1.forEach(function (item) {
    if (!object[item])
      object[item] = 0;
    object[item] += 1;
  })

  for (let prop in object) {
    if (object[prop] == 4) {
      result.push(prop);
    }
  }

  return result;

}

//Funcion para encontrar escaleras
function findStraightInArray(arra1) {
  let result = [];
  let cont = 0;
  for (let i = 0; i < arra1.length; i++) {
    if (arra1[i + 1] - arra1[i] === 1) {
      cont++;
    }
  }
  if (cont == 4) {
    result.push(5)
  }
  return result;
}

//Funcion para enontrar color
function findColorInArray(arra1) {
  let result = [];
  let cont = 0;
  for (let i = 0; i < arra1.length; i++) {
    if (arra1[i + 1] === arra1[i]) {
      cont++;
    }
  }
  if (cont == 4) {
   
    result.push(6);
  }
  return result;
}

//Funcion para encontrar full
function findFullInArray(arra1) {
  let result = [];
  let cont = 0;

  if (arra1[0] === arra1[1] && arra1[1] === arra1[2] && arra1[3] === arra1[4]) {
    cont = arra1[0];
  }
  if (arra1[0] === arra1[1] && arra1[2] === arra1[3] && arra1[3] === arra1[4]) {
    cont = arra1[2];
  }
  if (cont > 0) {
    result.push(cont)
  }
  return result;
}

//Funcion para encontrar la escalera de color
function findRoyalStraightInArray(arra1) {
  let result = [];
  let cont = 0;
  let arrayNumericValues = [];
  let arraySuitValues = [];

  for (let i = 0; i<arra1.length; i++ ) {
    arrayNumericValues.push(arra1[i].valor);
    arraySuitValues.push(arra1[i].palo);
  }

  if (findColorInArray(arraySuitValues).length > 0 && findStraightInArray(arrayNumericValues).length > 0) {
      result.push(9);
  }
  return result;
}


//Funcion para determinar el ganador segun la carta las alta
function winnerWithMaximumValueCard(miMapa) {
  let contadorMapa = 0;
  let personajeGanador = [];
  let arrayOfNumbers = [];
  console.log(miMapa);
  for (let [clave, valor] of miMapa.entries()) {
    arrayOfNumbers = valor.map(Number);
    if (parseInt(Math.max(...valor)) > contadorMapa) {
      contadorMapa = parseInt(Math.max(...valor));
      personajeGanador = clave;
    }else if(parseInt(Math.max(...valor)) == contadorMapa && contadorMapa != 0) {
      return 'Empate';
    }
  }
  //console.log(personajeGanador);
  
    return personajeGanador;
  
  
 
}

//Funcion para evitar valores vacios, menos/mas de 5 cartas, evitar valores negativos y cartas no validas
function comprobacionManos(mano) {
  let validCards = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"];
  let validSuits = ["C", "D", "H", "S"];
  let res = true;
  let stringCards = [];
  for (let i = 0; i < mano.length; i++) {
    if (mano[i].bote === "" ||
      mano[i].jugadas === "" ||
      isNaN(mano[i].bote)) {
      res = false;
    }
    for (let j = 0; j < mano[i].jugadas.length; j++) {
      if (mano[i].jugadas[j].jugador === "" ||
        mano[i].jugadas[j].apuesta === "" ||
        mano[i].jugadas[j].cartas === "" ||
        mano[i].jugadas[j].cartas.length != 5 ||
        isNaN(mano[i].jugadas[j].apuesta)) {
        res = false;
      }
      for (let k = 0; k < mano[i].jugadas[j].cartas.length; k++) {
        stringCards.push(mano[i].jugadas[j].cartas[k].valor + mano[i].jugadas[j].cartas[k].palo);
        if (!validCards.includes(mano[i].jugadas[j].cartas[k].valor) ||
          !validSuits.includes(mano[i].jugadas[j].cartas[k].palo) /*||
      findDuplicates(stringCards)*/) {
          res = false;
        }
      }
    }
  }
  return res;
}

function findDuplicates(stringCards) {
  let object = {};
  let result = [];

  stringCards.forEach(function (item) {
    if (!object[item])
      object[item] = 0;
    object[item] += 1;
  })

  for (let prop in object) {
    if (object[prop] == 2) {
      return true;
    } else {
      return false;
    }
  }
}

function comparar(a, b) { return a - b; }

module.exports.comprobacionManos = comprobacionManos;
module.exports.pair = pair;
